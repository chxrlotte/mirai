import { 
    GuildMember, 
    Client, 
    Message, 
    Collection, 
    MessageAttachment, 
    MessageEmbed, 
    MessageOptions,
    TextChannel,
    DMChannel,
    NewsChannel,
    MessageReaction, 
} from 'discord.js';

import { Bot } from './framework/bot';
import { Command } from './framework/command';

/**
 * Find a member in the current guild from a given snowflake
 * @param client
 * @param msg
 * @param snowflake
 */
export async function findMember(client: Client, msg: Message, snowflake: string): Promise<GuildMember> {
    const id = snowflake.replace(/<@!/g, '')
                        .replace(/>/g, '');
    const target = await client.users.fetch(id);
    const targetMember = msg.guild?.member(target);

    return targetMember!;
}

export type Channel = TextChannel | DMChannel | NewsChannel;

// More discord.js type mess
// eslint-disable-next-line
type Whatever = string | number | bigint | boolean | symbol | readonly any[] | (MessageOptions & {
    split?: false | undefined;
}) | MessageEmbed | MessageAttachment | (MessageEmbed | MessageAttachment)[];

/**
 * Send a message, then immediately follow it up with a reaction.
 * Reaction *must* be an unicode emoji string, not a :emoji: 
 * @param channel
 * @param content 
 * @param reaction
 */
export function sendThenReact(channel: Channel, content: Whatever, reaction: string): void {
    channel.send(content).then(msg => msg.react(reaction));
}

/**
 * Get all of the commands in a command group
 * @param client
 * @param module 
 */
export function getCommandsInGroup(client: Bot, group: string): Collection<string, Command> {
    const allCommands = client.commandLoader.commands;
    return allCommands.filter(c => c.properties.group == group);
}

/**
 * Return a string formatted in the format of username#discriminator
 * @param member
 */
export const getDiscordTag = (member: GuildMember): string =>
    `${member?.user.username}#${member?.user.discriminator}`;