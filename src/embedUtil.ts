import { MessageEmbed } from 'discord.js';

export enum Colors {
    normal = '#ff638f',
    warn = '#ff9463',
    error = '#ff6370',
}

/**
 * Helper function to wrap a message in a code block
 * @param message 
 */
export const wrapInBlock = (message: string): string => `\`\`\`${message}\n\`\`\``;

export function errorEmbed(message: string): MessageEmbed {
    const embed = new MessageEmbed()
        .setTitle('B-bbaka?')
        .setDescription('Something went wrong in the bot, and this embed was created!')
        .setColor(Colors.error)
        .addField('Reason', message);

    return embed;
}