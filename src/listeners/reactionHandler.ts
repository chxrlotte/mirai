import { Bot } from '../framework/bot';
import { Message, MessageReaction, User } from 'discord.js';

import { EventHandler } from '../framework/event';
import { CommandHandler } from './commandHandler';
import { logger } from '../framework/logging';

/**
 * Reaction event handler for the bot
 */
export class ReactionHandler extends EventHandler {
    constructor() {
        super();
    }
    
    execute(client: Bot, user: User, reaction: MessageReaction): void {
        if (user.bot)
            return;

        // Execute message hooks
        for (const [hookName, hook] of client.hookLoader.hooks) {
            if (hook.properties.trigger == 'reaction' && hook.properties.isEnabled) {
                // Execute the hook
                hook.executeHook(client, reaction.message, user, reaction, []);
            }
        }
    }
}