/**
 * Base Command class that other commands should derive from
 */
import { BitFieldResolvable, Message, PermissionString } from 'discord.js';
import { Bot } from './bot';

interface CommandProperties {
    name: string;
    aliases?: string[],
    group: string;
    usage?: string;
    description: string | 'No description provided';
    requiredPermissions?: [BitFieldResolvable<PermissionString>];
    ownerOnly: boolean;
}

export class Command {
    properties: CommandProperties;

    constructor(props: CommandProperties) {
        /* ctor */
        this.properties = props;
    }

    /**
     * Execute the command.
     * 
     * @param {*} client
     * @param {*} msg
     * @param {*} args
     */

    // eslint-disable-next-line
    execute(client: Bot, msg: Message, args: Array<string>): void {}
}