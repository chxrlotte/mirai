import { Message, MessageReaction, PartialUser, User } from "discord.js";
import { Bot } from "./bot";

// Whether the hook will trigger manually (not at all), on an interval,
// or when a message is recieved
export type HookTrigger = 'manual' | 'interval' | 'message' | 'reaction';

interface HookProperties {
    name: string;
    trigger: HookTrigger;
    interval?: number;
    isEnabled: boolean;
}

export class Hook {
    properties: HookProperties;

    constructor(props: HookProperties) {
        /* ctor */
        this.properties = props;
    }

    /**
     * Called when the hook is executed
     * 
     * @param {*} client the client (bot)
     * @param {*} msg the discord.js `Message` object that triggered the event
     * @param {*} args any additional arguments passed as an `object`
     */

    // eslint-disable-next-line
    executeHook(client: Bot, msg?: Message, user?: User | PartialUser | never[], reaction?: MessageReaction, args?: Array<string>): void {}
}