import { Hook } from "../hook";
import { Bot } from "../bot";
import { Collection } from "discord.js";

import { join } from 'path';
import { readdirSync, statSync } from "fs";
import { logger } from "../logging";

export class HookLoader {
    hooks: Collection<string, Hook>;
    paths: string[];

    constructor() {
        this.hooks = new Collection<string, Hook>();
        this.paths = [];
    }

    private walkDirectory(directory: string): string[] {
        // Walk a directory recursively

        const items = readdirSync(directory);
        for (const item of items) {
            const path = join(directory, item);

            if (path.endsWith('.hook.ts')) {                    
                // Found a hook, push it to the list
                this.paths.push(path.replace(/\.ts/, ''));
            }

            const stats = statSync(path);
            if (stats.isDirectory())
                this.walkDirectory(path);
        }

        return this.paths;
    }

    async loadHooks(client: Bot, directory: string): Promise<void> {
        const filePaths = this.walkDirectory(directory);

        for (const path of filePaths) {
            let hookName;

            try {
                const hookClass = await import(`${path}`);
                const hook: Hook = new hookClass.default();

                hookName = hook.properties.name;

                // Load the command
                logger.info(`loading hook ${hookName} (${path})`);

                this.hooks.set(hookName, hook);
                logger.info(`loaded hook ${hookName} successfully`);
            } catch (err) {
                logger.error(`failed to load hook ${hookName}`);
                logger.error(err);
            }
        }
    }
}